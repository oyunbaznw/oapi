package me.xXDavcerXx.API.Main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;

import com.CoinAPI.Main.CoinsAPI;

public class Achivement {
	
	private static HashMap<Byte, Achivement> achids = new HashMap<Byte, Achivement>(); 
	private static HashMap<String, Achivement> achnames = new HashMap<String, Achivement>();
	private static List<Achivement> allAchivements = new ArrayList<Achivement>();
	
	private byte id;
	private String name;
	private int coin;
	
	public Achivement(byte id, String name, int coin) {
		if(!achids.containsValue(id)){
			this.id = id;
			this.name = name;
			this.coin = coin;
			
			achids.put(id, this);
			achnames.put(name, this);
			allAchivements.add(this);
		}else{
			
		}
	}
	
	public static Achivement getAchivement(String name){
		if(achnames.containsKey(name)){
			return achnames.get(name);
		}else {
			return null;
		}
	}
	
	public static Achivement getAchivement(byte id){
		if(achids.containsKey(id)){
			return achids.get(id);
		}else {
			return null;
		}
	}
	public String getName(){
		return name;
	}
	
	public byte getID(){
		return id;
	}
	
	public void sendReward(Player p){
		CoinsAPI.addCoins(p, coin);
	}

}

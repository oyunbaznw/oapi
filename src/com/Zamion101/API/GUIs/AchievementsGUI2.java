package com.Zamion101.API.GUIs;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.Plugin;

import com.Zamion101.API.Utils.ChatManager;

import network.oyunbaz.API.Main.OAPI;

public class AchievementsGUI2
  implements Listener
{
  private Inventory inv;
  private Inventory inv2;
  private Inventory inv3;
  int time = 0;
  ArrayList<String> Lore = new ArrayList<String>();
  ArrayList<String> Lore2 = new ArrayList<String>();
  
  public AchievementsGUI2(Plugin pl)
  {
	  for (Player p : Bukkit.getOnlinePlayers()){
    inv = Bukkit.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', "&f&l[&e&lOyunbaz&6&lNW&f&l] &2&lBaşarımlar"));
    inv2 = Bukkit.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', "&f&l[&e&lOyunbaz&6&lNW&f&l] &2&lBaşarımlar &8&l2"));
    inv3 = Bukkit.getServer().createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', "&f&l[&e&lOyunbaz&6&lNW&f&l] &e&lSeçim Ekranı"));
    
    /*Ã–nceki Sayfa*/
    ItemStack Önceki = new ItemStack(Material.PAPER, 1);
    ItemMeta ÖncekiMeta = Önceki.getItemMeta();
   ÖncekiMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&8&l<< &e&lÖnceki Sayfa"));
   Önceki.setItemMeta(ÖncekiMeta);
    
    /*Kapat*/
    ItemStack close = new ItemStack(Material.WOOL, 1, (short)14);
    ItemMeta closeMeta = close.getItemMeta();
    closeMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&c&lKapat!"));
    close.setItemMeta(closeMeta);
    
    /*Sonraki Sayfa*/
    ItemStack sonraki = new ItemStack(Material.PAPER, 1);
    ItemMeta sonrakiMeta = sonraki.getItemMeta();
    sonrakiMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&8&l>> &e&lSonraki Sayfa") + OAPI.getInstance().getConfig().getString("Başarımlar.1"));
    sonraki.setItemMeta(sonrakiMeta);
    
    /*Oyunbaz*/
    ItemStack oyunbaz = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)14);
    ItemMeta oyunbazMeta = oyunbaz.getItemMeta();
    oyunbazMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&f&L[&e&lOyunbaz&6&lNW&f&l]"));
    oyunbaz.setItemMeta(oyunbazMeta);
    
    /*Test Başarım*/
    ItemStack test = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)14);
    ItemMeta testMeta = test.getItemMeta();
    testMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', OAPI.getInstance().getConfig().getString("Başarımlar.1")));
    test.setItemMeta(testMeta);
    
    /*Başarım Penceresi*/
    ItemStack başarım = new ItemStack(Material.EMERALD_BLOCK, 1);
    ItemMeta başarımMeta = başarım.getItemMeta();
    başarımMeta.setLore(this.Lore2);
    başarımMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&a&lBaşarımlar"));
    başarım.setItemMeta(başarımMeta);
    
    /*Oyuncu İstatistikleri*/
    ItemStack playerhead = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
    SkullMeta playerheadSkull = (SkullMeta)playerhead.getItemMeta();
    playerheadSkull.setOwner(p.getName());
    playerheadSkull.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&e&l" + p.getName() + "&a&l Istatistikleri" ));
    playerhead.setItemMeta(playerheadSkull);
    
    inv.setItem(49, oyunbaz);
    inv.setItem(50, sonraki);
    inv.setItem(53, close);
    
    if(p.hasPermission("OAPI.Admin")){
    inv.setItem(0, test);
    }
    
    inv2.setItem(48,Önceki);
    inv2.setItem(49, oyunbaz);
    
    inv3.setItem(49, playerhead);
    inv3.setItem(19, başarım);
  }
  }
  public void showInv(org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer p)
  {
    p.openInventory(inv);
  }
  
  public void showInv2(org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer p)
  {
    p.openInventory(inv2);
  }
  
  public void showmain(org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer p)
  {
    p.openInventory(inv3);
  }
  
  @EventHandler
  public void onClick(InventoryClickEvent e)
  {
	  org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer p = (org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer)e.getWhoClicked();
    
    /*Başarım 1*/
    if (e.getInventory().getName().equals(inv.getName()))
    {
      if (e.getCurrentItem() == null) {
        return;
      }
      else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Kapat!"))
      {
        e.setCancelled(true);
        e.getWhoClicked().closeInventory();
        if (e.isShiftClick())
        {
          e.setCancelled(true);
          p.closeInventory();
          p.openInventory(inv);
        }
        return;
      }
      else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Sonraki Sayfa"))
      {
        e.setCancelled(true);
        p.closeInventory();
        p.openInventory(inv2);
        if (e.isShiftClick())
        {
          e.setCancelled(true);
          p.closeInventory();
          p.openInventory(inv2);
        }
        return;
      }
      else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Oyunbaz"))
      {
        e.setCancelled(true);
        e.getWhoClicked().sendMessage(ChatManager.Normal() + "Bu Plugin Oyunbaz Network Adına Ã–zel Olarak " + ChatColor.GOLD + "Zamion101" + ChatColor.YELLOW + " & " + ChatColor.GREEN + " xXDavcerXx" + ChatColor.YELLOW + " Tarafından Kodlanmıştır!");
        if (e.isShiftClick())
        {
          e.setCancelled(true);
          p.closeInventory();
          p.openInventory(inv);
        }
        return;
      }
    }
    /*Başarım 2*/
    if (e.getInventory().getName().equals(inv2.getName()))
    {
      if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Önceki Sayfa"))
      {
        e.setCancelled(true);
        p.closeInventory();
        p.openInventory(inv);
        if (e.isShiftClick())
        {
          e.setCancelled(true);
          p.closeInventory();
          p.openInventory(inv);
        }
        return;
      }
      else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Oyunbaz"))
      {
        e.setCancelled(true);
        e.getWhoClicked().sendMessage(ChatManager.Normal() + "Bu Plugin Oyunbaz Network Adına Özel Olarak " + ChatColor.GOLD + "Zamion101" + ChatColor.YELLOW + " & " + ChatColor.GREEN + " xXDavcerXx" + ChatColor.YELLOW + " Tarafından Kodlanmıştır!");
        if (e.isShiftClick())
        {
          e.setCancelled(true);
          p.closeInventory();
          p.openInventory(inv2);
        }
        return;
      }
    }
    /*AnaSayfa*/
    if (e.getInventory().getName().equals(inv3.getName()))
    {
      if (e.getCurrentItem() == null) {
        return;
      }
      else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Başarımlar"))
      {
        e.setCancelled(true);
        p.closeInventory();
        p.openInventory(inv);
        if (e.isShiftClick())
        {
          e.setCancelled(true);
          p.closeInventory();
          p.openInventory(inv);
        }
        return;
      }
    }
  }
}

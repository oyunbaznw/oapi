package com.Zamion101.API.Utils;

import org.bukkit.ChatColor;

public class ChatManager{
	
	
	private static String dangerPrefix =  ChatColor.WHITE  + "[" + ChatColor.YELLOW + ChatColor.BOLD + 
			"Oyunbaz" + ChatColor.GOLD + "" + ChatColor.BOLD + "Network" + ChatColor.WHITE + "] " + ChatColor.DARK_RED;
	
	private static String errorPrefix = ChatColor.WHITE  + "[" + ChatColor.YELLOW + ChatColor.BOLD +
			"Oyunbaz" + ChatColor.GOLD + "" + ChatColor.BOLD + "Network" + ChatColor.WHITE + "] " + ChatColor.RED;
	
	private static String successPrefix = ChatColor.WHITE  + "[" + ChatColor.YELLOW + ChatColor.BOLD +
			"Oyunbaz" + ChatColor.GOLD + "" + ChatColor.BOLD + "Network" + ChatColor.WHITE + "] " + ChatColor.GREEN;
	
	private static String normalPrefix = ChatColor.WHITE  + "[" + ChatColor.YELLOW + ChatColor.BOLD +
			"Oyunbaz" + ChatColor.GOLD + "" + ChatColor.BOLD + "Network" + ChatColor.WHITE + "] ";
	
	
	
	public static String Error(){
		return errorPrefix;
	}
	
	public static String Danger(){
		return dangerPrefix;
	}
	
	public static String Success(){
		return successPrefix;
	}
	
	public static String Normal(){
		return normalPrefix;
	}
	
//	public static String ErorrMessage(String message){
//		return OAPI.getInstance().getLogger().log(Level.WARNING, message);
//	}

}

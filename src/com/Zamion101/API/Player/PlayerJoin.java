package com.Zamion101.API.Player;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.Zamion101.API.Utils.ChatManager;

import net.md_5.bungee.api.ChatColor;

public class PlayerJoin implements Listener{

	
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		String pname = p.getName();
		
		e.setJoinMessage(null);
		
		p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED , 1000000, 0, false, false));
		p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP , 1000000, 1, false, false));
		
		p.sendMessage(ChatManager.Normal() + ChatColor.BOLD + "Sunucumuza Hoşgeldin" + ChatColor.YELLOW + ChatColor.BOLD + pname + ChatColor.WHITE + ChatColor.BOLD + "!");
	}
}

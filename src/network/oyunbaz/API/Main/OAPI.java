 package network.oyunbaz.API.Main;
 
 import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.Zamion101.API.GUIs.AchievementsGUI;
import com.Zamion101.API.GUIs.AchievementsGUI2;
import com.Zamion101.API.License.AdvancedLicense;
 
 public class OAPI extends JavaPlugin implements Listener{
 	
 	public static OAPI instance;
 	
 	private AchievementsGUI gui;
 	private AchievementsGUI2 gui2;
 	
 	public static OAPI getInstance(){
 		return instance;
 	}
 
 	public void onEnable(){
 		if(!new AdvancedLicense(getConfig().getString("License_Code"), "http://oyunbaz.network/license/verify.php", this).register()) return;
 	//Test//
 		
 		instance = this;
 		gui = new AchievementsGUI(this);
 		gui2 = new AchievementsGUI2(this);
 		if(getVersion() == "1.10"){
 		registerEvents(this, this, new AchievementsGUI(this));
 		}else{
 			registerEvents(this, this,new AchievementsGUI2(this));
 		}
 	}
 	
 	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
 		for (Listener listener : listeners) {
 		Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
 		}
 		}
 	
 	
 	public void onDisable(){
 		instance = null;
 	}
 	
 	public void setError(String s, boolean shutdown){
 		if(shutdown){
 			getServer().getLogger().log(Level.WARNING, s, true);
 			Bukkit.shutdown();
 		}else {
 			getServer().getLogger().log(Level.WARNING, s, true);
 		}
 	}
 	
 	@Override
 	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer oyuncu1 = (org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer) sender;
		org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer player = (org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer) sender;
 		
 			if(cmd.getName().equalsIgnoreCase("Başarım")){
 				if(getVersion() == "1.10"){
 				gui.showmain((org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer) player);
 			}
 				else if(getVersion() == "1.8"){
 					gui2.showmain((org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer) oyuncu1);
 				}
 				}
 			
 		return false;
 	}
 	
 	public String getVersion(){
 		if(Bukkit.getBukkitVersion().contains("1.8")){
 			return "1.8";
 		}else if(Bukkit.getBukkitVersion().contains("1.9")){
 			return "1.9";
 		}else if(Bukkit.getBukkitVersion().contains("1.10")){
 			return "1.10";
 		}else {
 			return null;
 		}
 	}
 
 }